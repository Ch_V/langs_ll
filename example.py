"""Primitive usage example of internationalization package langs."""

from langs import ll, LL  # [l]oca[l]ization with only one language loaded
print('ll: ', ll, end='\n\n')


LJ = 20  # str.ljust(LJ) output parameter

print('"a" in "en":'.ljust(LJ), ll['a'])
print('"b" in "en":'.ljust(LJ), ll.b)

ll.lang = 'en-GB'
print('"a" in "en-GB":'.ljust(LJ), ll.a)
print('"b" in "en-GB":'.ljust(LJ), ll['b'])

ll.lang = 'ru'
print('"a" in "ru":'.ljust(LJ), ll['a'])
print('"b" in "ru":'.ljust(LJ), ll['b'])


# IDENTICAL TRANSFORMATION
ll.lang = 'identical'
print('"a" in "identical":'.ljust(LJ), ll['a'])
print('"b" in "identical":'.ljust(LJ), ll['b'], end='\n\n')


# CONTEXT MANAGER
print('"a" before contexts:'.ljust(LJ), ll['a'])
with ll.lc('en-GB'):
    print('"a" into context1:'.ljust(LJ), ll['a'])
    with ll.lc('ru'):
        print('"a" into context2:'.ljust(LJ), ll['a'])
print('"a" after contexts:'.ljust(LJ), ll['a'])
