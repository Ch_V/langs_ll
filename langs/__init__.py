"""LANGS - Internationalization package with localisation modules.

Names of modules should be like `lang-code.py` or something similar as you want.
(Yes, you can use `-` or even `space` symbols here).
Each module should contain localization dictionary named 'lang'.

Package should be imported as 'from langs import ll' (don't run it as '__main__').
ll (called `ll` - because of [L]oca[L]izer) should be used as following:
    `ll.lang -> 'lang-code'` - to get current localization language
    `ll.lang = 'lang-code'` - to set localization language
    `ll['place-code']` or `ll.place_code`(slower) - to extract localized text for the place
    `with ll.lc('lang-code'): ...` - localization language context manager

If you want to encode places where you use internationalized text with corresponding text in one of
languages you can specify `lang` as empty dictionary in corresponding language localization module.
This will result in `ll['text'] -> 'text'` when `ll.lang` is set to this language.
"""

from contextlib import contextmanager
# import gc
import glob
import importlib
import os
import sys
# import weakref; wrs = []          # FOR-`test_clear`-TEST-ONLY


class LL:
    """LocaLizer construction class with loadable from modules localization data.
            `ll.lang -> 'lang-code'` - to get current localization language
            `ll.lang = 'lang-code'` - to set localization language
            `LL.langs()` or `ll.langs()` - to get available localization modules (lang-codes)
            `ll['place-code']` or `ll.place_code`(slower) - to extract localized text for the place
            `with ll.lc('lang-code'): ...` - localization language context manager

    Parameters
    ----------
    default_language : str, optional
        default language code - used when ll.lang is being set with False-like value (`ll.lang = ''`)
        Default is 'en'.

    strict : bool, optional
        Determines if ModuleNotFoundError should be raised (or just printed in stderr).
        Default is False (just print and rollback).

    clear : bool, optional
        Determines if unused localization data should be cleaned.
        Frees some memory but slows down `ll.lang` setting.
        Default is False.
    """

    __slots__ = ('default_language', 'strict', 'clear', '_lang_code', '_ll')

    __version__ = '1.1'

    def __init__(self, default_language: str = 'en', strict: bool = False, clear: bool = False):
        super().__init__()
        self.default_language = default_language
        self.strict = strict  # should ModuleNotFoundError be raised (or just printed in stderr)
        self.clear = clear  # should (not current language) localization data be cleared
        self._lang_code = ''  # keeps old `lang_code` through `set_lang` call for rollback ability
        self._ll = {}  # localization dictionary for current language; empty for identical transformation
        try:
            self.lang = default_language
        except ModuleNotFoundError as e:
            langs = self.langs()
            if langs:
                self.lang = self.default_language = langs[0]
                print("WARNING: ", e, f" '{langs[0]}' set as default ", file=sys.stderr, sep='')
            else:
                raise ModuleNotFoundError('ERROR: No localization modules found!')


    @property
    def lang(self):
        """lang-code of language currently set for localization."""
        return self._lang_code

    @lang.setter
    def lang(self, lang_code: str):
        if not lang_code:
            self.lang = self.default_language
            return None

        try:
            new_ll = importlib.import_module('.'.join((__package__, lang_code))).lang
        except ModuleNotFoundError as e:
            if self.strict:
                raise e
            elif lang_code != self.default_language:
                print('WARNING: ', e, file=sys.stderr, sep='')
                self.lang = self._lang_code
                return None
            else:
                print('WARNING: ', e, file=sys.stderr, sep='')
                raise ModuleNotFoundError('Default language module not found!')

        # global wrs                                          # FOR-`test_clear`-TEST-ONLY
        # try:                                                # FOR-`test_clear`-TEST-ONLY
        #     wr = weakref.proxy(sys.modules.get(             # FOR-`test_clear`-TEST-ONLY
        #         '.'.join((__package__, self._lang_code))))  # FOR-`test_clear`-TEST-ONLY
        #     wrs.append(wr)                                  # FOR-`test_clear`-TEST-ONLY
        # except (KeyError, TypeError):                       # FOR-`test_clear`-TEST-ONLY
        #     pass                                            # FOR-`test_clear`-TEST-ONLY

        self._ll = new_ll
        self._lang_code = lang_code

        if self.clear:
            _old_localization_module_name = '.'.join((__package__, self._lang_code))
            del sys.modules[_old_localization_module_name]
            _globals = globals()
            try:
                del _globals[_old_localization_module_name.partition('.')[-1]]
            except KeyError:
                pass

            # gc.collect()  # do we really want to do it manually? in test cases it slows down performance very much.

    def __getitem__(self, item):
        if len(self._ll):
            return self._ll.__getitem__(item)
        else:
            return item

    def __getattr__(self, item):
        if item == '__dict__':
            return None
        return self[item]

    def __repr__(self):
        return f"<langs.LL: lang='{self.lang}', default_language='{self.default_language}', " \
               f"strict={self.strict}, clear={self.clear}>"

    @staticmethod
    def langs():
        """Return available localization modules of this package"""
        available_languages = []

        for filename in glob.iglob(__path__[0] + '/*.py'):
            filename = os.path.basename(filename)[:-3]
            if filename != '__init__':
                available_languages.append(filename)

        return available_languages


    @contextmanager
    def lc(self, lang: str = ''):
        """Language Context manager."""
        _lang = self.lang
        self.lang = lang
        yield
        self.lang = _lang


ll = LL(default_language='en', strict=False, clear=False)
