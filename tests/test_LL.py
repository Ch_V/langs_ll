"""Tests for `langs` internationalization package.
This tests strictly depend on distributed examples of localization modules.
"""

import sys; sys.path.append(sys.path[0] + '/..')
import timeit

import pytest

from langs import LL

lngs = ['en', 'en-GB', 'ru', 'identical']
a_s = ['Hello!', 'Hello! What a good weather is today!', 'Привет!', 'a']
b_s = ['Goodbye!', 'Goodbye! It seems to start raining soon!', 'Пока!', 'b']


@pytest.mark.parametrize('clear', [False, True])
def test_basic_functionality(clear):
    """Test of basic functionality, including:
        - creation with default_language
        - setting with default_language
        - change of language
        - __getitem__ - query
        - __getattr__ - query
        - identical transformation query
        - basic `clear` parameter work (for full test of `clear` parameter see `test_clear` test below).
    """
    ll = LL(default_language='en', clear=clear)
    assert ll['a'] == a_s[lngs.index('en')]
    ll.lang = 'en-GB'
    assert ll['a'] == a_s[lngs.index('en-GB')]
    ll.default_language = 'identical'
    assert ll.a == a_s[lngs.index('en-GB')]
    ll.lang = 'ru'
    assert ll.b == b_s[lngs.index('ru')]
    ll.lang = ''
    assert ll.c == 'c'
    assert ll['qwe'] == 'qwe'

    if clear:
        for m in ['en', 'en-GB', 'ru']:
            assert 'langs.' + m not in sys.modules
    else:
        for m in ['en', 'en-GB', 'ru', 'identical']:
            assert 'langs.' + m in sys.modules


@pytest.mark.parametrize('clear', [False, True])
@pytest.mark.parametrize('lng, a, b, idx', zip(lngs, a_s, b_s, range(4)))
def test_more_grid(lng, a, b, idx, clear):
    """Grid combination test of different conditions
    setting `.lang` and `default_language`.
    """
    ll = LL(default_language=lng, clear=clear)
    assert ll['a'] == a
    assert ll['b'] == b

    idx_rotated = divmod(len(lngs), idx + 1)[-1]  # just idx list rotation
    ll.lang = lngs[idx_rotated]
    assert ll['a'] == a_s[idx_rotated]
    assert ll['b'] == b_s[idx_rotated]


def test_wrong_default_language(capsys):
    """Test of setting wrong default language."""
    ll = LL(default_language='qwe')
    assert capsys.readouterr().err.startswith(
           "WARNING: No module named 'langs.qwe'\n"
           "WARNING: Default language module not found! 'en-GB' set as default")
    assert ll.default_language == 'en-GB'


def test_strictness(capsys):
    """Test of strict parameter."""
    ll = LL(strict=False)
    ll.lang = 'en'
    ll.lang = 'qwe'
    assert capsys.readouterr().err.startswith("WARNING: No module named 'langs.qwe'")
    assert ll['a'] == a_s[lngs.index('en')]  # rollback assertion
    ll.strict = True
    with pytest.raises(ModuleNotFoundError):
        ll.lang = 'qwe'


def test_context():
    """Test of ll.language_context() context manager."""
    ll = LL(default_language='en')
    assert ll['a'] == a_s[lngs.index('en')]
    with ll.lc('en-GB'):
        assert ll['a'] == a_s[lngs.index('en-GB')]
        with ll.lc('ru'):
            assert ll['a'] == a_s[lngs.index('ru')]
        assert ll['a'] == a_s[lngs.index('en-GB')]
    assert ll['a'] == a_s[lngs.index('en')]


def test_langs():
    """Test of method LL.langs."""
    langs = LL.langs()
    ll = LL()
    assert langs == ll.langs()
    assert len(langs) == 4
    for l in ['en-GB', 'en', 'identical', 'ru']:
        assert l in langs


@pytest.mark.skip(reason='Uncomment all lines marked as "# FOR-`test_clear`-TEST-ONLY" in `__init__.py` to run this test')
@pytest.mark.parametrize('clear', [False, True])
def test_clear(clear):
    """Test of clearing of non-current language localization data when clear set to True in ll.
    !!! Uncomment all lines marked as "# FOR-`test_clear`-TEST-ONLY" in `__init__.py` to run this test.
    """
    from langs import wrs
    wrs.clear()
    ll = LL(default_language='en', clear=clear)

    ll.lang = 'en-GB'
    ll.lang = 'ru'
    ll.lang = 'identical'

    with ll.lc('en-GB'):
        pass
        with ll.lc('ru'):
            pass
    assert ll.lang == 'identical'

    if not clear:
        for i in wrs:  # en, en-GB, ru, identical, en-GB, ru, en-GB
            i.lang
    else:
        for i in wrs:
            with pytest.raises(ReferenceError):
                i.lang


######################### PERFORMANCE TESTS #########################

def change_performance():
    """For timeit measurement of language change performance."""
    ll = LL(default_language='en', clear=False)
    for i in range(10 ** 2):
        ll.lang = 'en-GB'
        ll.lang = 'ru'
        ll.lang = 'en'
        ll.lang = 'identical'


def get_performance():
    """For timeit measurement of localized text extraction performance."""
    ll = LL(default_language='en')
    for i in range(10 ** 2):
        _ = ll['a']
        _ = ll['a']
        _ = ll['a']
        _ = ll['a']
        # _ = ll.a
        # _ = ll.a
        # _ = ll.a
        # _ = ll.a


if __name__ == '__main__':
    # SET
    print('SET:',
          min([round(x, 4) for x in timeit.repeat(change_performance, repeat=3, number=20)])
          )
    #  0.0961   advanced
    #  3.8926   advanced + clear
    # 29.9068   advanced + clear + forced gc.collect()

    # GET
    print('GET:',
          min([round(x, 4) for x in timeit.repeat(get_performance, repeat=3, number=20)])
          )
    # 0.0004    simple
    # 0.0015    advanced ll['a']
    # 0.0037    advanced ll.a
