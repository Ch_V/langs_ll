# LANGS internationalization package
Very small, simple and lightweight Python3 package for programs internationalization / localization.
Consists of two versions (placed on two branches):
- **simple** - very lightweight simple and fast implementation based on dictionaries.
- **advanced** - a little more flexible and functional version, but it is slower 
  especially on language change. See **Performance notes** below.
  
## Overview
Both packages have nearly the same structure:\
`example.py`\
`langs/`\
&nbsp;&nbsp;&nbsp;&nbsp;`|- __init__.py`\
&nbsp;&nbsp;&nbsp;&nbsp;`|- en.py`\
&nbsp;&nbsp;&nbsp;&nbsp;`|- ...other localization modules...`
\
\
*advanced* version contains also directory with pytest tests called `tests`.\
`example.py` contains primitive examples of usage.\
Folder `langs` is package itself. It contains `__init__.py` and examples of localization modules.

## Usage
You mostly don't need to modify `__init__.py`, but you should create your own localization language modules
in this package named like `lang-code.py` or something similar as you want (yes, you can use `-` or even
`space` symbols here). Language module should contain localization dictionary called `lang` where key
encodes place you want to use internationalized text and value is localized text in this module
specified language.

This package should be imported, don't run it as `__main__`.\
No other modules except `__init__.py` and localization modules allowed inside this package.

### Simple version (on branch `simple`)
First import localizer `ll` (called `ll` - because of '**L**oca**L**izer) from `langs` package as
`from langs import ll`.
Then you can use it as `ll['lang-code']['place-code'] -> localized text`. Some little more advanced
examples are shown in `examples.py`.

If you want to encode places where you use internationalized text with corresponding text in one of
languages you can specify `lang` as `ReturnKey()` in corresponding language localization module
(just uncomment lines). This will result in `ll['this-lang-code']['text'] -> 'text'` behaviour.


### Advanced (on branch `advanced`)
First import localizer `ll` (called `ll` - because of '**L**oca**L**izer) from `langs` package as
`from langs import ll`.
You may want to customize `ll = LL(some-parameters)` in last line of `__init__.py` of this package.

Then you can use `ll` as following:\
`ll.lang -> 'lang-code'` - to get current localization language\
`ll.lang = 'lang-code'` - to set localization language\
`ll['place-code']` or `ll.place_code`(slower) - to extract localized text for the place\
`with ll.lc('lang-code'): ...` - localization language context manager

If you want to encode places where you use internationalized text with corresponding text in one of
languages you can specify `lang` as *empty dictionary* in corresponding language localization module.
This will result in `ll['text'] -> 'text'` when `ll.lang` is set to this language.

## Performance notes
In most practical cases you will never notice that *advanced* version is slower. Although it is much slower
on language change (especially with `clear=True`) which is relatively rarely occurs during most programs operation,
and just a little slower on localized text values extraction.

Furthermore, *advanced* version may use less memory resources (especially with `clear=True`) due to
this version unlike *simple* version doesn't load all localization data in memory.

Finally, all this performance / memory differences seems to be insignificant in most practical cases.

You can look for some basic performance tests in `example.py` for *simple* version 
and in `tests/test_LL.py` for *advanced* version.
