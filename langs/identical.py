"""Localization module of internationalization package `langs`.
It should contain localization dictionary lang = {place_code: localized_text}.

If you want to encode places where you use internationalized text with corresponding text in this
language you can specify `lang` as empty dictionary. This will result in `ll['text'] -> 'text'`
when `ll.lang` is set to this language.
"""

lang = {}
